/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fsm.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/18 16:29:55 by ccano             #+#    #+#             */
/*   Updated: 2014/02/18 16:30:04 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FSM_H
# define FSM_H

# include <libft.h>

# define ERR_MALLOC		-1
# define ERR_NULL		-2

# define NB_STATE	9

# define ERR_LEX	-2

# define T_INFILE	1
# define T_PIPE		2
# define T_REDI		3
# define T_REDO		4
# define T_SPACE	5
# define T_SEP		6

typedef struct		s_lexem
{
   char				*lexem;
   int				type;
   int				parsed;
   struct s_lexem	*next;
   struct s_lexem	*prev;
}					t_lexem;


typedef struct		s_fsm
{
  const char	*states;
  char			**mtx_trans;
  int			(**ft_fsm)(struct s_fsm *fsm);
  char			*b_lexem;
  char			*current;
  int			state;
  int			old_state;
  int			type;
  t_lexem		*s_lexem;
  t_lexem		*s_blexem;
}					t_fsm;

typedef		int (**t_ft_fsm)(struct s_fsm *s_fsm);

int			fsm_lex(char **mtx_trans, const char *states, 
					char *line, t_lexem **begin);
int			del_s_space(t_lexem **s_blexem);
int			new_lexem(t_lexem **lex);
int			new_lexem_pushback(t_fsm *s_fsm);
int			del_lexem_elem(t_lexem *lexem, t_lexem **begin);

int			fsm_bof(t_fsm *s_fsm);
int			fsm_eof(t_fsm *s_fsm);
int			fsm_new_lex(t_fsm *s_fsm);
int			fsm_inside_lex(t_fsm *s_fsm);
int			fsm_error(t_fsm *s_fsm);

#endif

