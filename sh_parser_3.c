/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sh_parser_3.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/18 15:27:47 by ccano             #+#    #+#             */
/*   Updated: 2014/02/18 16:47:44 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh_parser.h>

void		parse_fatal(t_parser *s_parser)
{
	printf("Parse Error at %s\n", s_parser->s_lexem->lexem);
	free(s_parser);
}

int			parse_sep(t_parser *s_parser)
{
	printf("\n");
	s_parser->s_lexem = s_parser->s_lexem->next;
	return (1);
}

int			parse_eof(void)
{
	printf("\n");
	return (0);
}

int			parse_init(t_parser *s_parser)
{
	if (!s_parser->s_lexem)
		return (parse_eof());
	if (s_parser->s_lexem->type == S_ALPHA)
	{
		if ((s_parser->s_lexem->next) && 
				(s_parser->s_lexem->next->type == S_REDI))
			return (parse_ril(s_parser));
		else
			return (parse_cmd(s_parser));
	}
	else if (s_parser->s_lexem->type == S_REDI)
		return (parse_rir(s_parser));
	printf("Parse Error at %s\n", s_parser->s_lexem->lexem);
	return (-1);
}

