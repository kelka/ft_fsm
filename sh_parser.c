/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sh_parser.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/18 15:24:07 by ccano             #+#    #+#             */
/*   Updated: 2014/02/18 16:40:43 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh_parser.h>

static int		parser_init_env(t_parser **s_parser, t_lexem *s_blexem)
{
	int			ret;

	ret = 0;
	if ((*s_parser = (t_parser *)malloc(sizeof(**s_parser))))
	{
		(*s_parser)->s_blexem = s_blexem;
		(*s_parser)->s_lexem = s_blexem;
		(*s_parser)->s_ast = NULL;
		ret = 1;
	}
	return (ret);
}

int				sh_parser(t_lexem *s_blexem, t_parser **s_rparser)
{
	t_ast		*s_bast;
	t_parser	*s_parser;
	int			ret;

	ret = -1;
	if ((parser_init_env(&s_parser, s_blexem)) 
			&& (ret = 1) && !(s_bast = NULL))
	{
		while ((s_parser->s_lexem) && (ret > 0))
		{
			printf("ok\n");
			if (!(s_parser->s_ast = ast_new_pushback(&s_bast)))
				return (-1);
			ret = parse_init(s_parser);
		}
	}
	*s_rparser = s_parser;
	return (ret);
}

