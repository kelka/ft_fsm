/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fsm.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/18 16:29:04 by ccano             #+#    #+#             */
/*   Updated: 2014/02/18 16:29:29 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fsm.h>

int				del_s_space(t_lexem **s_blexem)
{
	t_lexem		*s_nlexem;
	t_lexem		*s_lexem;
	int			ret;

	ret = 0;
	if (s_blexem && (ret = 1))
	{
		s_lexem = *s_blexem;
		while (s_lexem)
		{
			if (s_lexem->type == T_SPACE)
			{
				s_nlexem = s_lexem->next;
				del_lexem_elem(s_lexem, s_blexem);
				s_lexem = s_nlexem;
			}
			else
				s_lexem = s_lexem->next;
		}
	}
	return (ret);
}

static int		init_ft_fsm(t_fsm *s_fsm)
{
	t_ft_fsm		*ft_fsm;

	if (!(ft_fsm = (t_ft_fsm *)ft_memalloc(sizeof(*ft_fsm) * 5)))
		return (0);
	ft_fsm[0] = &fsm_bof;
	ft_fsm[1] = &fsm_new_lex;
	ft_fsm[2] = &fsm_inside_lex;
	ft_fsm[3] = &fsm_eof;
	ft_fsm[4] = &fsm_error;
	s_fsm->ft_fsm = ft_fsm;
	return (1);
}

int				fsm_lex(char **mtx_trans, const char *states, char *line, 
		t_lexem **begin)
{
	t_fsm	*s_fsm;
	int		next;
	int		ret;

	if (!begin)
		return (-1);
	if (!(s_fsm = (t_fsm *)ft_memalloc(sizeof(*s_fsm))))
		return (-1);
	s_fsm->states = states;
	s_fsm->old_state = 0;
	s_fsm->current = line;
	s_fsm->mtx_trans = mtx_trans;

	if (!(init_ft_fsm(s_fsm)))
	{
		free(s_fsm);
		//FREE MTX
		return (-1);
	}
	s_fsm->state = (int)(s_fsm->states[(int)(s_fsm->current[0])]);
	next = s_fsm->mtx_trans[s_fsm->old_state][s_fsm->state];
	ret = s_fsm->ft_fsm[next](s_fsm);
	if (ret > -1)
		*begin = s_fsm->s_blexem;
	else
		*begin = NULL;
	return (ret);
}

