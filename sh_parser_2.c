/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sh_parser_2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/18 15:22:42 by ccano             #+#    #+#             */
/*   Updated: 2014/02/18 19:56:34 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh_parser.h>

int		parse_pipe(t_parser *s_parser)
{
	// t_ast_binary node
	printf("piped to: ");
	s_parser->s_lexem = s_parser->s_lexem->next;
	if (s_parser->s_lexem && (s_parser->s_lexem->type == S_ALPHA))
		return (parse_cmd(s_parser));
	else
		parse_fatal(s_parser);
	return (-4);
}

int		parse_ro(t_parser *s_parser)
{
	s_parser->s_lexem = s_parser->s_lexem->next;
	if (s_parser->s_lexem)
	{
		// t_ast_unary node
		printf("outfile: %s ", s_parser->s_lexem->lexem);
		s_parser->s_lexem = s_parser->s_lexem->next;
		if (!s_parser->s_lexem)
			return (parse_eof());
		if (s_parser->s_lexem && (s_parser->s_lexem->type == S_SEP))
			return (parse_sep(s_parser));
	}
	parse_fatal(s_parser);
	return (-3);
}

int		parse_cmd(t_parser *s_parser)
{
	char	**cmd;

	printf("cmd: %s, args: ", s_parser->s_lexem->lexem);
	s_parser->s_lexem = s_parser->s_lexem->next;
	while (s_parser->s_lexem && (s_parser->s_lexem->type == S_ALPHA))
	{
		printf("%s ", s_parser->s_lexem->lexem);
		s_parser->s_lexem = s_parser->s_lexem->next;
	}
	if (s_parser->s_lexem && (s_parser->s_lexem->type == S_REDO))
		return (parse_ro(s_parser));
	if (s_parser->s_lexem && (s_parser->s_lexem->type == S_SEP))
		return (parse_sep(s_parser));
	if (s_parser->s_lexem && (s_parser->s_lexem->type == S_PIPE))
		return (parse_pipe(s_parser));
	if (!s_parser->s_lexem)
		return (parse_eof());
	parse_fatal(s_parser);
	printf("\n");
	return (-5);
}

int		parse_rir(t_parser *s_parser)
{
	// t_ast_unary node
	s_parser->s_lexem = s_parser->s_lexem->next->next;
	printf("Infile: %s ", s_parser->s_lexem->lexem);
	s_parser->s_lexem = s_parser->s_lexem->next;
	if (s_parser->s_lexem && (s_parser->s_lexem->type == S_ALPHA))
		return (parse_cmd(s_parser));
	parse_fatal(s_parser);
	return (-3);
}

int		parse_ril(t_parser *s_parser)
{
	// construct s_ast_unary
	printf("Infile: %s ", s_parser->s_lexem->lexem);
	s_parser->s_lexem = s_parser->s_lexem->next->next;
	if (s_parser->s_lexem && (s_parser->s_lexem->type == S_ALPHA))
		return (parse_cmd(s_parser));
	parse_fatal(s_parser);
	return (-2);
}

