/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ast_build.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/18 18:29:38 by ccano             #+#    #+#             */
/*   Updated: 2014/02/19 14:25:54 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh_parser.h>


t_ast_node	*build_node(void)
{
	t_ast_node	*s_node;

	if ((s_node = (t_ast_node *)malloc(sizeof(*s_node))))
	{
		s_node->type = -1;
		s_node->prev = NULL;
		s_node->left = NULL;
		s_node->right = NULL;
	}
	return (s_node);
}

int			build_leaf(t_parser *s_parser, t_ast_leaf *s_leaf)
{
	int				ret;
	t_ast_node		*left;

	if (s_parser->s_ast->expr && (s_parser->s_ast->expr->type == P_BINARY))
	{
		left = s_parser->s_ast->expr->node->left;
		if (!(s_parser->s_ast->expr->node->left) && (ret = 1))
		{
			// CHECK IF NULL
			left = build_node();
			left->type = P_LEAF;
			left->node->prev = (void *)s_parser->s_ast->expr->node;
		}
		else if (!(s_parser->s_ast->expr->right && (ret = 1)))
			s_parser->s_ast->expr->right = s_leaf;
		else
			ret = -7;
	}
	else if (!s_parser->s_ast->expr)
	{
		s_parser->s_ast->expr = build_node();
		s_parser->s_ast->expr.type = P_LEAF;
		s_parser->s_ast->expr->node = s_leaf;
		ret = 1;
	}
	else
		ret = -7;
	return (ret);
}
/*
int			build_binary(t_parser *s_parser, t_ast_binary *s_bin)
{
	int		ret;

	if (s_parser->s_ast->expr && (s_parser->s_ast->type == P_BINARY))
	{
		if (!(s_ast->expr.s_bin.left) && (ret = 1))
			s_parser->s_ast
	}
}
*/
