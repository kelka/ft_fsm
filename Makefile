# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: ccano <ccano@student.42.fr>                +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/02/18 15:29:18 by ccano             #+#    #+#              #
#    Updated: 2014/02/18 19:15:15 by ccano            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = sh_3
SRC = fsm.c fsm_ft.c lexem_list.c sh_lexer.c \
	  ft_ast.c sh_parser.c sh_parser_2.c sh_parser_3.c \
	  ast_build.c
OBJ = $(SRC:.c=.o)
CFLAGS = -Wall -Wextra -ggdb3

all: $(NAME)

$(NAME):
	cc $(CFLAGS) $(SRC) -I. -I$(HOME)/lib/libft -L$(HOME)/lib/libft -lft

clean:
	rm -rf $(OBJ)

fclean: clean
	rm -rf $(NAME)

re: fclean all
	
