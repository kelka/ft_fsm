/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexem_list.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/18 16:31:50 by ccano             #+#    #+#             */
/*   Updated: 2014/02/18 16:32:43 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fsm.h>

int				new_lexem(t_lexem **lex)
{
	if (!lex)
		return (ERR_NULL);
	if (!(*lex = (t_lexem *)ft_memalloc(sizeof(**lex))))
		return (0);
	(*lex)->lexem = NULL;
	(*lex)->type = 0;
	(*lex)->parsed = 0;
	(*lex)->next = NULL;
	(*lex)->prev = NULL;
	return (1);
}

int				new_lexem_pushback(t_fsm *s_fsm)
{
	t_lexem		*p_lexem;

	if (!s_fsm)
		return (ERR_NULL);
	if (!(new_lexem(&s_fsm->s_lexem)))
		return (ERR_MALLOC);
	if (s_fsm->s_blexem)
	{
		p_lexem = s_fsm->s_blexem;
		while (p_lexem->next)
			p_lexem = p_lexem->next;
		p_lexem->next = s_fsm->s_lexem;
		s_fsm->s_lexem->prev = p_lexem;
	}
	else
		s_fsm->s_blexem = s_fsm->s_lexem;
	return (1);
}


int				del_lexem_elem(t_lexem *lexem, t_lexem **begin)
{
	int			ret;
	t_lexem		*pbegin;

	if (begin && (lexem != *begin))
	{
		lexem->prev->next = lexem->next;
		if (lexem->next)
			lexem->next->prev = lexem->prev;
		free(lexem);
		ret = 1;
	}
	else if (begin && (lexem == *begin))
	{
		pbegin = (*begin)->next;
		(*begin)->next->prev = NULL;
		free(*begin);
		*begin = pbegin;
		ret = 1;
	}
	else
		ret = ERR_NULL;
	return (ret);
}

/* Doesn't free lexem (data) field of t_lexem elem */

