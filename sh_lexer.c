/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sh_lexer.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/18 16:33:11 by ccano             #+#    #+#             */
/*   Updated: 2014/02/18 16:38:11 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh_lexer.h>
//printf
#include <stdio.h>

#define un __attribute__ ((unused))

static int			init_states(char *states)
{
	int				n;

	n = -1;
	while (++n < 127)
	{
		if (ft_isalnum(n) || (n == '-'))
			states[n] = S_ALPHA;
		else if (n == '<')
			states[n] = S_REDI;
		else if (n == '>')
			states[n] = S_REDO;
		else if (n == '|')
			states[n] = S_PIPE;
		else if ((n == ' ') || (n == '\t'))
			states[n] = S_SPACE;
		else if (n == ';')
			states[n] = S_SEP;
		else if (n == '\0')
			states[n] = S_END;
		else
			states[n] = S_ELSE;
	}
	/*
	   if (n < 126)
	   return (-1);
	   else
	   */
	return (1);
}

static int			sh_mtx(char *line, t_lexem **s_blexem)
{
	char			*states;
	int				ret;
	int				n;
	char			**mtx_trans;
	const char		mtx_state[NB_STATE][NB_STATE] =
	{
		//	   I  A	 <  >  | ' ' ; '/0' else
		{4, 0, 0, 4, 4, 4, 4, 3, 4}, // I
		{4, 2, 1, 1, 1, 1, 1, 3, 4}, // A
		{4, 1, 2, 4, 4, 1, 4, 4, 4}, // <
		{4, 1, 4, 2, 4, 1, 4, 4, 4}, // >
		{4, 1, 4, 4, 2, 1, 4, 4, 4}, // |
		{4, 1, 1, 1, 1, 2, 1, 3, 4}, // ' '
		{4, 1, 4, 4, 4, 1, 2, 3, 4}, // ;
		{4, 4, 4, 4, 4, 4, 4, 4 ,4}, // '\0' 
		{4, 4, 4, 4, 4, 4, 4, 4, 4}, // else
	};
	// 0 : BOF
	// 1 : new lex
	// 2 : inside lex
	// 3 : EOF
	// 4 : Error
	// 5 : NOP

	if ((n = -1) && !(mtx_trans = (char **)ft_strnew(sizeof(*mtx_trans) * NB_STATE)))
		return (-1);
	while (++n < NB_STATE)
	{
		if (!(mtx_trans[n] = ft_strnew(sizeof(char **) * NB_STATE)))
			return (-1);
		mtx_trans[n] = (char *)ft_memcpy(mtx_trans[n], mtx_state[n], 
				sizeof(char *) * (NB_STATE));
	}
	if (!(states = ft_strnew(sizeof(*states) * 127)))
		return (-1);
	if (!(init_states(states)))
		return (-2);
	ret = fsm_lex(mtx_trans, states, line, s_blexem);
	free(states);
	return (ret);
}

int					sh_lexer(char *line, t_lexem **s_rlexem)
{
	int				ret;
	t_lexem			*s_blexem;
	t_lexem			*s_plexem;

	ret = -2;
	if (line && *line)
		ret = sh_mtx(line, &s_blexem);
	if (ret > 0)
		ret = del_s_space(&s_blexem);
	s_plexem = s_blexem;
	while (s_plexem)
	{
		printf("lexem: %s\ttype: %d\n", s_plexem->lexem, s_plexem->type);
		s_plexem = s_plexem->next;
	}
	*s_rlexem = s_blexem;
	return (ret);
}

int					main(int un argc, char **argv)
{
#include <sh_parser.h>
	int				ret;
	t_lexem			*s_lexem;
	t_parser			*s_parser;

	if (argc == 2)
	{
		ret = sh_lexer(argv[1], &s_lexem);
		ft_putnbr(ret);
		ft_putchar('\n');
		printf("s_lexem: %p\n", s_lexem);
		if (ret > 0)
		{
			ret = sh_parser(s_lexem, &s_parser);
			ft_putnbr(ret);
			ft_putchar('\n');
		}
	}
	return (ret);
}
