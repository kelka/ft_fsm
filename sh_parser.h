/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sh_parser.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/18 16:42:41 by ccano             #+#    #+#             */
/*   Updated: 2014/02/19 14:28:00 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SH_PARSER_H
# define SH_PARSER_H

# include <sh_lexer.h>

//printf include
# include <stdio.h>

# define P_LEAF		1
# define P_UNARY	2
# define P_BINARY	3

/*
 * General AST structs
 */

typedef struct			s_ast_node
{
	int					type;
	struct s_ast_node	*prev;
	struct s_ast_node	*left;
	struct s_ast_node	*right;

}						t_ast_node;

typedef struct			s_ast
{
	t_ast_node		*expr;
	struct s_ast	*next;
	struct s_ast	*prev;
}						t_ast;

/*
 * Parser env struct
 */

typedef struct			s_parser
{
	t_lexem			*s_blexem;
	t_lexem			*s_lexem;
	t_ast			*s_ast;
}						t_parser;

/*
 * AST build functions
 */

t_ast				*ast_new_pushback(t_ast	**s_bast);
t_ast_leaf			*new_ast_leaf(void);
t_ast_unary			*new_ast_unary(void);
t_ast_binary		*new_ast_binary(void);


/*
 * Parse functions
 */

int					sh_parser(t_lexem *s_blexem, t_parser **s_parser);
int					parse_init(t_parser *s_parser);
int					parse_ril(t_parser *s_parser);
int					parse_rir(t_parser *s_parser);
int					parse_cmd(t_parser *s_parser);
int					parse_ro(t_parser *s_parser);
int					parse_pipe(t_parser *s_parser);
int					parse_eof(void);
int					parse_sep(t_parser *s_parser);
void				parse_fatal(t_parser *s_parser);

#endif

